# Display settings:(in px)
size = windowWidth, windowHeight = 700, 700
framesPerSecond = 120
showEntityDirection = False
entityDirectionLineMultiplier = 20 # basically how big the line is

# UI settings
infoBarWidth = 300
UIElementLRMargin = 20 # left right margin
UIElementTBMargin = 10 # top bottom margin
UIElementHeight = 40
displayAllDef = True # wether or not to display all sprites by default

# Population Generation Settings (INITIAL POPULATION)
popSize = 40
startScore = 0
sizeRange = [20, 20]
speedRange = [1, 10] # this is the range for x and for y (INITIAL SPEED)
detectionParam = 300 # in pixels (for x and y) - how far can sprite see (creates a square around sprite) - this num should be even
colourImageRange = [1, 12] # choose from 12 different images for the sprite
xSpawnRange = [0, windowWidth - sizeRange[0]]
ySpawnRange = [0, windowHeight - sizeRange[1]]

# Sprite movement settings
xStep = 1 # how much x and y speed values change per frame (acceleration)
yStep = 1
xMax = 10 # max speed in x and y direction
yMax = 10
xMin = -10 # min speed in x and y direction 
yMin = -10

# Environment physics settings
frictionVelocityMultiplier = 0.95 # Multiplied by velocity. Generally, keep same for X and Y
frictionVelocityMultiplier = 0.95
coeffRestitutionWall = 0.3  # 0 = no bounce (sticky), 1 = velocity conserved (0.5 = default)

# Checkpoint settings
numCheckpoints = 15
checkpointSize = 3 # radius
checkpointColour = (255, 255, 255)
checkpointPurgedColour = (128, 128, 128)
xCPSpawnRange = [windowWidth/10, (windowWidth/10)*9] # coordinate spawn range for checkpoints
yCPSpawnRange = [windowHeight/10, (windowHeight/10)*9]

# Scoring settings
spriteCollisionScore = 500  # how much to increase score by when there is a collision
# each element is a certain distance from a checkpoint, and if the sprite is below that distance, it will receive the corresponding score which is the key pair
# must be in increasing order
spriteProximityScores = {20 : 240, 40: 120, 60: 60, 120 : 30} 

# NN Settings
inputNodes = 9
hiddenNodes = 32
hiddenNodes2 = 32
outNodes = 5

# GA Settings
trainForGenerations = 1000000
parentSprites = 2 # must be enabled if crossover is enabled
mutationNDistMean = 0
mutationNDistStdDev = 0.25
trialsPerGen = 3
ticksPerTrial = 400
crossover = True

