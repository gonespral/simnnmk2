import sys
import os
import time
import random
import math
import psutil
import copy
import pickle
from PIL import Image as im # for handling sprite images 
# resource files
import config as cfg
from resources import title
# pygame for GUI
import pygame
from pygame_gui import UIManager
from pygame_gui.elements import UILabel
from pygame_gui.elements import UISelectionList
from pygame_gui.elements import UIDropDownMenu
# import intelligence
import neuralnetwork
import numpy as np

spriteInfo = {} # keeps track of the position of all the sprites for the current tick 
# SPRITEINFO DICT ORDER: spawnCoords(0), size(1), speed(2), spawnSpeed(3), detection(4), colourImage(5), score(6), isBeingDisplayed(7)
# this dictionary keeps track of useful information about each sprite, assigned a unique ID when being generated

checkpointInfo = {} # keeps track of the position of all checkpoints
# CHECKPOINT INFO ORDER: spawnCoords(0), list of sprites that have collided with it(1), dict of sprites that have come within range, and their range(2) - ranges defined in cfg.spriteProximityScores
# this dictionary keeps track of useful information about each checkpoint, assigned a unique ID when being generated.

currentSprite = None # currently selected sprite to be drawn
clockTicks = 0
generationNum = 0
trialNum = 0
displayAll = cfg.displayAllDef # display all sprites by default or not
topScoreLastGen = 0
averageScoreLastGen = 0

class Checkpoint(pygame.sprite.Sprite):
    def __init__(self, spawnCoords, checkpointID):
        pygame.sprite.Sprite.__init__(self)  # need this for sprites to work correctly in pygame
        self.spawnCoords = spawnCoords
        self.checkpointID = checkpointID
    
        # from config file
        self.size = cfg.checkpointSize
        self.checkpointSurface = pygame.Surface((cfg.checkpointSize*2, cfg.checkpointSize*2), pygame.SRCALPHA)
        pygame.draw.circle(self.checkpointSurface, cfg.checkpointColour, (cfg.checkpointSize, cfg.checkpointSize), cfg.checkpointSize)
        self.image = self.checkpointSurface
        self.rect = self.image.get_rect(center=self.spawnCoords)

    def purge(self):
        pygame.draw.circle(self.checkpointSurface, cfg.checkpointPurgedColour, (cfg.checkpointSize, cfg.checkpointSize), cfg.checkpointSize)
        self.image = self.checkpointSurface
        self.rect = self.image.get_rect(center=self.spawnCoords)

    def unpurge(self):
        pygame.draw.circle(self.checkpointSurface, cfg.checkpointColour, (cfg.checkpointSize, cfg.checkpointSize), cfg.checkpointSize)
        self.image = self.checkpointSurface
        self.rect = self.image.get_rect(center=self.spawnCoords)

    def update(self):
        if currentSprite in checkpointInfo[self.checkpointID][1]: # if sprite being displayed has collided, then purge the checkpoint
            self.purge()
        else:
            self.unpurge()


class Sprite(pygame.sprite.Sprite):
    def __init__(self, size, speed, detection, colourImage, spawnCoords, startScore, spriteID, weightInit, savedNN):
        pygame.sprite.Sprite.__init__(self) # need this for sprites to work correctly in pygame
        self.spriteID = spriteID

        # function input vars
        self.size = size
        self.spawnSpeed = copy.deepcopy(speed)
        self.speed = speed
        self.detection = detection
        self.score = startScore
        self.imageFile = f"resources/images/b ({colourImage}).gif"
        self.image = pygame.image.load(self.imageFile) 
        self.imageDimensions = im.open(self.imageFile).size # open image and get dimensions
        self.imageHWRatio = (self.imageDimensions[1]/self.imageDimensions[0]) # calculate H/W ratio, to rescale properly
        self.image = pygame.transform.scale(self.image, (self.size, int(self.imageHWRatio*self.size))) # rescale image to the defined size
        self.rect = self.image.get_rect()
        self.rect.x = spawnCoords[0]
        self.rect.y = spawnCoords[1]
        self.bearing = 0 
        self.isTouchingWallL = 0
        self.isTouchingWallR = 0
        self.isTouchingWallT = 0
        self.isTouchingWallB = 0
        self.deltaBearing = 0

        self.currentPos = (self.rect.topleft) # get coords of top left corner of rect
        
        if savedNN: 
            self.nn = savedNN
            print(f"[*] Restored weights for spriteID:{self.spriteID}")
        else: 
            self.nn = neuralnetwork.NN()
            if len(weightInit) > 0 and not cfg.crossover: 
                self.nn.constructorWithInitWeight(cfg.inputNodes, cfg.hiddenNodes, cfg.hiddenNodes2, cfg.outNodes, weightInit)
            elif len(weightInit) > 0 and cfg.crossover:
                self.nn.constructorCrossover(cfg.inputNodes, cfg.hiddenNodes, cfg.hiddenNodes2, cfg.outNodes, weightInit)
            else: 
                self.nn.constructor(cfg.inputNodes, cfg.hiddenNodes, cfg.hiddenNodes2, cfg.outNodes)
                print(f"[*] Initialized spriteID:{self.spriteID} with random weights")

    def bounce(self): 
        # use self.rect.y to change rect position, since self.rect.topleft only reads the positional values (returns tuple, immutable)
        if self.currentPos[0] <= 0: # left wall
            self.rect.x = 0
            self.speed[0] = -self.speed[0]*cfg.coeffRestitutionWall
            self.isTouchingWallL = 1
        elif self.currentPos[0] + self.size >= cfg.windowWidth: # right wall
            self.rect.x = cfg.windowWidth - self.size
            self.speed[0] = -self.speed[0]*cfg.coeffRestitutionWall
            self.isTouchingWallR = 1
        elif self.currentPos[1] <= 0: # top wall
            self.rect.y = 0
            self.speed[1] = -self.speed[1]*cfg.coeffRestitutionWall
            self.isTouchingWallT = 1
        elif self.currentPos[1] + self.size >= cfg.windowHeight: # bottom wall
            self.rect.y = cfg.windowHeight - self.size
            self.speed[1] = -self.speed[1]*cfg.coeffRestitutionWall
            self.isTouchingWallB = 1
        else:
            self.isTouchingWallL = 0
            self.isTouchingWallR = 0
            self.isTouchingWallT = 0
            self.isTouchingWallB = 0
            
    def friction(self): # depends on velocity of the sprite ()
        self.speed[0] = self.speed[0]*cfg.frictionVelocityMultiplier
        self.speed[1] = self.speed[1]*cfg.frictionVelocityMultiplier

    def changeDirection(self, direction):
        if direction == "up":
            self.speed[1] -= cfg.yStep
            if self.speed[1] < cfg.yMin:
                self.speed[1] = cfg.yMin
        elif direction == "down":
            self.speed[1] += cfg.yStep
            if self.speed[1] > cfg.yMax:
                self.speed[1] = cfg.yMax
        elif direction == "left":
            self.speed[0] -= cfg.xStep
            if self.speed[0] < cfg.xMin:
                self.speed[0] = cfg.xMin
        elif direction == "right":
            self.speed[0] += cfg.xStep
            if self.speed[0] > cfg.xMax:
                self.speed[0] = cfg.xMax
        elif direction == "none":
            pass

    def updateScore(self): # checks for collision with checkpoint
        spriteCoords = self.rect.center
        for i in checkpointInfo:
            if self.spriteID not in checkpointInfo[i][1]:
                checkpointCoords = checkpointInfo[i][0]
                #distance between centres
                distance = math.sqrt((spriteCoords[0]-checkpointCoords[0])**2+(spriteCoords[1]-checkpointCoords[1])**2) # py-tha-go-ras!
                rectDiagonal = math.sqrt((self.size/2)**2 + (self.size/2)**2)
                if distance < cfg.checkpointSize + rectDiagonal: # if collision
                    self.score += cfg.spriteCollisionScore
                    checkpointInfo[i][1].append(self.spriteID)
                else:
                    # the following loop rewards sprites for approaching checkpoints
                    # the score is defined in cfg.spriteProximityScore
                    # if a sprite enters within range, it will receive a score, but this only happens once per checkpoint
                    for x in cfg.spriteProximityScores: 
                        if distance <= x: # if its within range
                            if self.spriteID in checkpointInfo[i][2]:
                                if checkpointInfo[i][2][self.spriteID] < x:
                                    break
                                elif checkpointInfo[i][2][self.spriteID] > x:
                                    self.score += cfg.spriteProximityScores[x]
                                    checkpointInfo[i][2][self.spriteID] = x
                            checkpointInfo[i][2][self.spriteID] = x

    def draw(self, screen):
        screen.blit(self.image, self.rect)

    def drawEntityDirection(self, screen):
        centreSprite = (self.currentPos[0] + self.size/2, self.currentPos[1] + self.size/2)
        endLineCoords = (self.currentPos[0] + self.speed[0]*cfg.entityDirectionLineMultiplier, self.currentPos[1] + self.speed[1]*cfg.entityDirectionLineMultiplier)
        pygame.draw.aaline(screen, (255, 0, 0), centreSprite, endLineCoords) #anti aliased line, to show direction of entity
        self.bearing = self.calcBearing((self.currentPos[0],self.currentPos[1]), tuple(endLineCoords))

    def calcBearing(self, pointA, pointB):
        # Calculates the bearing between two points
        # parameters must be tuples

        lat1 = math.radians(pointA[0])
        lat2 = math.radians(pointB[0])

        diffLong = math.radians(pointB[1] - pointA[1])

        x = math.sin(diffLong) * math.cos(lat2)
        y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1) * math.cos(lat2) * math.cos(diffLong))

        initialBearing = math.atan2(x, y)

        initialBearing = math.degrees(initialBearing)
        compassBearing = (initialBearing + 360) % 360
        return compassBearing

    def getSurroundingsAlt(self): # just testing with different types of inputs for the NN.
        cpDistances = {}  # cpID : [relative distance]
        for checkpoint in checkpointInfo:  # get distance to every checkpoint
            if self.spriteID not in checkpointInfo[checkpoint][1]:
                cpx, cpy = checkpointInfo[checkpoint][0][0], checkpointInfo[checkpoint][0][1]
                distanceToCP = math.hypot(self.currentPos[0]-cpx, self.currentPos[1]-cpy)
                cpDistances.update({checkpoint: distanceToCP})
        if cpDistances:  # select closest CP and get its ID and normalised distance, store in cpID and cpDistance
            cpID = min(cpDistances, key=cpDistances.get)
            cpDistance = cpDistances[cpID]
            cpDistance = cpDistance/math.hypot(cfg.windowHeight, cfg.windowWidth)  # normalize

            cpx, cpy = checkpointInfo[cpID][0][0], checkpointInfo[cpID][0][1] 
            cpDeltaX, cpDeltaY = (self.currentPos[0]-cpx)/cfg.windowWidth, (self.currentPos[1]-cpy)/cfg.windowHeight # this is normalized too

        spriteSpeed = spriteInfo[self.spriteID][2]
        spriteSpeed = [spriteSpeed[0]/self.spawnSpeed[0], spriteSpeed[1]/self.spawnSpeed[1]] # normalize
    
        # find distance to the nearest wall (Left and Top are negative)
        LWall = self.rect.center[0]
        RWall = cfg.windowWidth - self.currentPos[0]
        TWall = self.rect.center[1]
        BWall = cfg.windowHeight - self.currentPos[1]
        RL = [LWall, RWall]
        TB = [TWall, BWall]
        shortestDistIndexRL = RL.index(min(RL))
        shortestDistIndexTB = TB.index(min(TB))
        if shortestDistIndexRL == 0:
            wallDistRL = -RL[shortestDistIndexRL]
            wallDistRL = wallDistRL/cfg.windowWidth
        elif shortestDistIndexRL == 1:
            wallDistRL = RL[shortestDistIndexRL]
            wallDistRL = wallDistRL/cfg.windowWidth
        if shortestDistIndexTB == 0:
            wallDistTB = -TB[shortestDistIndexTB]
            wallDistTB = wallDistTB/cfg.windowHeight
        elif shortestDistIndexTB == 1:
            wallDistTB = TB[shortestDistIndexTB]
            wallDistTB = wallDistTB/cfg.windowHeight

        return [cpDistance, cpDeltaX, cpDeltaY, spriteSpeed[0], spriteSpeed[1], self.isTouchingWallL, self.isTouchingWallR, self.isTouchingWallT, self.isTouchingWallB]

    def getSurroundings(self):
        cpDistances = {} # cpID : [relative distance]
        for checkpoint in checkpointInfo: # get distance to every checkpoint
            if self.spriteID not in checkpointInfo[checkpoint][1]:
                cpx, cpy = checkpointInfo[checkpoint][0][0], checkpointInfo[checkpoint][0][1]
                distanceToCP = math.hypot(self.currentPos[0]-cpx,self.currentPos[1]-cpy)
                cpDistances.update({checkpoint:distanceToCP})
        if cpDistances: # select closest CP and get its ID and normalised distance, store in cpID and cpDistance
            cpID = min(cpDistances, key=cpDistances.get)
            cpDistance = cpDistances[cpID]
            cpDistance = cpDistance/math.hypot(cfg.windowHeight, cfg.windowWidth) # normalize

            cpBearing = self.calcBearing((self.currentPos[0],self.currentPos[1]), (checkpointInfo[cpID][0][0], checkpointInfo[cpID][0][1]))
            cpBearing = cpBearing/360 # normalize
        else:
            cpBearing = 0 # have to define this if there are no more checkpoints left, otherwise these vars are left undefined and rest of code breaks
            cpDistance = 0 

        spriteSpeed = spriteInfo[self.spriteID][2]
        spriteSpeed = [spriteSpeed[0]/self.spawnSpeed[0], spriteSpeed[1]/self.spawnSpeed[1]] # normalize
        spriteBearing = self.bearing # updated every tick in self.drawEntityDirection()
        spriteBearing = spriteBearing/360 # normalize

        deltaBearing = spriteBearing - cpBearing 

        # find distance to the nearest wall (Left and Top are negative)
        LWall = self.rect.center[0]
        RWall = cfg.windowWidth - self.currentPos[0]
        TWall = self.rect.center[1]
        BWall = cfg.windowHeight - self.currentPos[1]
        RL = [LWall, RWall]
        TB = [TWall, BWall]
        shortestDistIndexRL = RL.index(min(RL))
        shortestDistIndexTB = TB.index(min(TB))      
        if shortestDistIndexRL == 0:
            wallDistRL = -RL[shortestDistIndexRL]
            wallDistRL = wallDistRL/cfg.windowWidth
        elif shortestDistIndexRL == 1:
            wallDistRL = RL[shortestDistIndexRL]
            wallDistRL = wallDistRL/cfg.windowWidth
        if shortestDistIndexTB == 0:
            wallDistTB = -TB[shortestDistIndexTB]
            wallDistTB = wallDistTB/cfg.windowHeight
        elif shortestDistIndexTB == 1:
            wallDistTB = TB[shortestDistIndexTB]
            wallDistTB = wallDistTB/cfg.windowHeight

        return [cpDistance, deltaBearing, spriteSpeed[0], spriteSpeed[1], wallDistRL, wallDistTB, cfg.isTouchingWall]

    def update(self, moveDirection):
        self.rect = self.rect.move(self.speed)
        # get coords of top left corner of rect
        self.currentPos = (self.rect.topleft)
        self.bounce() 
        self.friction()

        prediction = self.nn.predict(self.getSurroundingsAlt())[0].numpy()
        x = np.where(prediction == max(prediction))
        if x[0][0] == 0:
            self.changeDirection("up")
            self.score -= 5
        elif x[0][0] == 1:
            self.changeDirection("down")
            self.score -= 5
        elif x [0][0]== 2:
            self.changeDirection("left")
            self.score -= 5
        elif x[0][0] == 3:
            self.changeDirection("right")
            self.score -= 5
        elif x[0][0] == 4:
            self.changeDirection("none")
            self.score -= 4

        #self.changeDirection(moveDirection) # for keyboard control

        self.updateScore()
        spriteInfo[self.spriteID][0] = self.rect.topleft # Update position in spritePositions dictionary 
        spriteInfo[self.spriteID][2] = self.speed # Update speed
        spriteInfo[self.spriteID][6] = self.score # Update score

class UI(): 
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode((cfg.windowWidth + cfg.infoBarWidth, cfg.windowHeight))
        pygame.display.set_caption("Simulating Natural Selection MK2")
        self.clock = pygame.time.Clock()
        clockTicks = 0

        # UI Management
        self.running = True # UIManager()
        self.ui_manager = UIManager((cfg.windowWidth + cfg.infoBarWidth, cfg.windowHeight))

        # Event variables
        self.moveDirection = None

    def generateUIElements(self): # pygame.Rect(coordsleft, coordstop, width, height) <- just in case i wanna add more stuff
        # when adding a new element, make sure to check the coordinates of where the pygame.Rect is placed, and use margins defined in config.py
        self.fpsCounter = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), cfg.UIElementTBMargin, cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), "FPS = None", self.ui_manager, object_id="#fpsCounter")
        self.popSizeUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*2 + cfg.UIElementHeight), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"cfg.popSize = {cfg.popSize}", self.ui_manager, object_id="popSizeUI")
        self.numCheckpointsUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*3 + cfg.UIElementHeight*2),cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"cfg.numCheckpoints = {cfg.numCheckpoints}", self.ui_manager, object_id='#numCheckpointsUI')
        self.cpuUsageUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*4 + cfg.UIElementHeight*3), cfg.infoBarWidth -cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"Ticks = None", self.ui_manager, object_id='#totalTicks')
        self.totalTicksUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*5 + cfg.UIElementHeight*4), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"cfg.popSize = {cfg.popSize}", self.ui_manager, object_id="popSizeUI")
        self.allSpriteIDs = []
        for i in spriteInfo:
            self.allSpriteIDs.append(i)
        global currentSprite
        currentSprite = self.allSpriteIDs[0]
        spriteInfo[currentSprite][7] = True
        self.currentSpriteUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*6 + cfg.UIElementHeight*5), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"spriteID : {currentSprite}", self.ui_manager, object_id="currentSpriteUI")
        self.currentSpriteScoreUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*7 + cfg.UIElementHeight*6), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"spriteScore : 0", self.ui_manager, object_id="currentSpriteScoreUI")
        self.currentGenerationUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*8 + cfg.UIElementHeight*7), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"currentGen : {generationNum}", self.ui_manager, object_id="currentGenScoreUI")
        self.currentTrialUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*9 + cfg.UIElementHeight*8), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"currentTrial : {trialNum}/{cfg.trialsPerGen - 1}", self.ui_manager, object_id="currentTrialUI")
        self.topScoreLastGenerationUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*10 + cfg.UIElementHeight*9), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"topScorePrev : {topScoreLastGen}", self.ui_manager, object_id="topScoreLastGenUI")

        self.NNStructureUI = UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*11 + cfg.UIElementHeight*10), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"{cfg.inputNodes}->{cfg.hiddenNodes}->{cfg.hiddenNodes2}->{cfg.outNodes}", self.ui_manager, object_id="currentSpriteScoreUI")
        self.keyInfo2 =  UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*13 + cfg.UIElementHeight*13), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"[SPACE] : show all", self.ui_manager, object_id="keyInfo")
        self.keyInfo1 =  UILabel(pygame.Rect((cfg.windowWidth + cfg.UIElementLRMargin), (cfg.UIElementTBMargin*12 + cfg.UIElementHeight*12), cfg.infoBarWidth - cfg.UIElementLRMargin*2, cfg.UIElementHeight), f"[ARROW_K] : change view", self.ui_manager, object_id="keyInfo")

    def updateUIElements(self):
        self.fpsCounter.set_text(f"FPS = {round(self.clock.get_fps(), 3)}/{cfg.framesPerSecond}")
        if clockTicks % 30 == 1: # every how many ticks to refresh CPU usage (otherwise, it isn't readable) 
            self.cpuUsageUI.set_text(f"CPU = {psutil.cpu_percent()}%")
        self.totalTicksUI.set_text(f"TICKS = {clockTicks}")
        self.currentSpriteUI.set_text(f"spriteID : {currentSprite}")
        self.currentSpriteScoreUI.set_text(f"spriteScore : {spriteInfo[currentSprite][6]}")
        self.currentGenerationUI.set_text(f"currentGen: {generationNum}")
        self.currentTrialUI.set_text(f"currentTrial : {trialNum}/{cfg.trialsPerGen - 1}")
        self.topScoreLastGenerationUI.set_text(f"topScorePrev : {topScoreLastGen}")

    def process_events(self):
        global currentSprite
        global displayAll

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
                self.quitSafe()
            elif event.type == pygame.KEYDOWN:
                
                '''
                if event.key == pygame.K_w:
                    self.moveDirection = "up"
                elif event.key == pygame.K_s:
                    self.moveDirection = "down"
                elif event.key == pygame.K_a:
                    self.moveDirection = "left"
                elif event.key == pygame.K_d:
                    self.moveDirection = "right" 
                '''

                if event.key == pygame.K_SPACE:
                    displayAll = True
                    for sprite in spriteInfo:
                        spriteInfo[sprite][7] = True
                elif event.key == pygame.K_UP: # these two conditional statements scroll through the different sprites available to visualize
                    displayAll = False
                    for sprite in spriteInfo:
                        spriteInfo[sprite][7] = False
                    currentIndex = self.allSpriteIDs.index(currentSprite)
                    spriteInfo[currentSprite][7] = False # disable last sprite
                    nextIndex = currentIndex + 1
                    if nextIndex > len(self.allSpriteIDs) - 1: # makes sure the index is a valid one (don't display sprite 20 if it doesn't exist)
                        nextIndex = len(self.allSpriteIDs) - 1
                    currentSprite = self.allSpriteIDs[nextIndex]
                    spriteInfo[currentSprite][7] = True # enable new sprite
                elif event.key == pygame.K_DOWN:
                    displayAll = False
                    for sprite in spriteInfo:
                        spriteInfo[sprite][7] = False
                    currentIndex = self.allSpriteIDs.index(currentSprite)
                    spriteInfo[currentSprite][7] = False
                    nextIndex = currentIndex - 1
                    if nextIndex < 0:
                        nextIndex = 0
                    currentSprite = self.allSpriteIDs[nextIndex]
                    spriteInfo[currentSprite][7] = True

            elif event.type == pygame.KEYUP:
                self.moveDirection = None

    def quitSafe(self):
        print("[!] Saving data to file")
        structure = [cfg.inputNodes, cfg.hiddenNodes, cfg.hiddenNodes2, cfg.outNodes]
        NNData = []
        for sprite in allSprites: NNData.append(sprite.nn)
        with open("savedData.pk", "wb") as f:
            pickle.dump([structure, cfg.popSize, generationNum, NNData], f)
            # saves list with a bunch of data to pk file, for next run. List is in order:
            # NNStructure (0), populationSize (1), generationNum (2), NNWeights (3)
        print("[!] sys.exit()")

        sys.exit()
    
    def run(self):
        global clockTicks
        global generationNum
        global trialNum
        spriteWeights = []

        try:
            generateCheckpoints()
            for i in range(cfg.trainForGenerations): # do this for every generation
                print(f"[*] Starting generation {generationNum}")
                print(f"[*] Top score last generation: {topScoreLastGen}")
                with open("score.txt", "a") as w:
                    w.write(f"{topScoreLastGen}\n")
                    w.write(f"{averageScoreLastGen}\n\n")
                if os.path.exists("savedData.pk") and generationNum == 0: 
                    print("[!] Pickle file found. Restoring weights.")
                    with open("savedData.pk", "rb") as f:
                        data = pickle.load(f)
                        # the following conditionals make sure the data in the pickle file are compatible with the variables in config.py
                        if data[0] != [cfg.inputNodes, cfg.hiddenNodes, cfg.hiddenNodes2, cfg.outNodes]:
                            print("[!] NN structure in pickle file incompatible")
                            print(f"[!] Network structure must be {data[0]}")
                            print("[!] sys.exit()")
                            sys.exit()
                        if data[1] != cfg.popSize:
                            print("[!] Population size in pickle file incompatible")
                            print(f"[!] Size must be: {data[1]}")
                            print("[!] sys.exit()")
                            sys.exit()

                        # if all is OK, continue: 
                        generatePopulationFromFile(data[3])

                        if generationNum == 0: self.generateUIElements()

                        generationNum = data[2] # adjust generation number (saved in pk file)

                else:
                    if cfg.crossover:
                        if spriteWeights: 
                            generatePopulationWithCrossover([spriteWeights[0], spriteWeights[1]]) # passes top two weights from last generation (empty for first generation)
                        else:
                            generatePopulationWithCrossover([])
                    else: 
                        generatePopulation(spriteWeights) # passes top weights from last generation (empty list for first gen)

                    if generationNum == 0: self.generateUIElements()

                generateCheckpoints()

                # do this for each trial
                for i in range(cfg.trialsPerGen):
                    self.running = True
                    clockTicks = 0
                    trialNum = i

                    if i >= 0: randomizeSpritePositions()
                    if i >= 0: 
                        for checkpoint in checkpointInfo: 
                            checkpointInfo[checkpoint][1] = []

                    while self.running:
                        self.clock.tick(cfg.framesPerSecond)
                        clockTicks += 1
                        self.timeDelta = self.clock.tick() / 1000.0

                        self.process_events()  # keyboard input and such

                        # calculate physics, and update parameters
                        for sprite in allSprites: sprite.update(self.moveDirection)
                        allCheckpoints.update()

                        # draw new position of all entities
                        self.screen.fill((40, 40, 40))
                        allCheckpoints.draw(self.screen)
                        for sprite in allSprites:  # only draws sprites which are supposed to be visible
                            if spriteInfo[sprite.spriteID][7] == True: # if visible
                                sprite.draw(self.screen)
                                if cfg.showEntityDirection == True:
                                    sprite.drawEntityDirection(self.screen)

                        # update ui elements (PygameGUI)
                        self.updateUIElements()
                        self.ui_manager.update(self.timeDelta)
                        self.ui_manager.draw_ui(self.screen)

                        #flip display
                        pygame.display.flip()

                        if clockTicks == cfg.ticksPerTrial: self.running = False

                # do this at end of gen
                bestSpriteIDs = getBestSprites()
                spriteWeights = []
                for sprite in allSprites:
                    if sprite.spriteID in bestSpriteIDs:
                        spriteWeights.append(sprite.nn.getWeights()) # this is then passed to a generator function at the start of this loop

                generationNum += 1

            self.quitSafe()
        
        except KeyboardInterrupt:
            self.quitSafe()

def getBestSprites(): # returns list containing top spriteIDs (see cfg GA Settings)
    global topScoreLastGen
    global averageScoreLastGen
    spriteScores = {}
    for i in spriteInfo:
        spriteScores.update({i : spriteInfo[i][6]})
    topScoreLastGen = spriteScores[max(spriteScores, key=spriteScores.get)]
    
    scores = []
    # get average
    for i in spriteScores:
        scores.append(spriteScores[i])
    averageScoreLastGen = int(sum(scores)/len(scores))

    return sorted(spriteScores, key=spriteScores.get, reverse=True)[:cfg.parentSprites] # get top n (cfg.parentSprites) sprites    

def randomizeSpritePositions():
    global allSprites

    xCoord = random.randint(cfg.xSpawnRange[0], cfg.xSpawnRange[1])
    yCoord = random.randint(cfg.ySpawnRange[0], cfg.ySpawnRange[1])
    speedX = random.randint(cfg.speedRange[0], cfg.speedRange[1])
    speedY = random.randint(cfg.speedRange[0], cfg.speedRange[1])

    for sprite in allSprites:
        speed = [speedX, speedY]

        sprite.rect.x = xCoord
        sprite.rect.y = yCoord
        sprite.speed = speed

def generatePopulation(weightInit): # randomly generate an initial population with random traits
    global allSprites
    global spriteInfo
    weightIndexRand = 0
    spriteInfo = {}
    allSprites = pygame.sprite.Group() # creates sprite group allSprites

    xSpawn = random.randint(cfg.xSpawnRange[0], cfg.xSpawnRange[1])
    ySpawn = random.randint(cfg.ySpawnRange[0], cfg.ySpawnRange[1])
    speedX = random.randint(cfg.speedRange[0], cfg.speedRange[1])
    speedY = random.randint(cfg.speedRange[0], cfg.speedRange[1])

    for i in range(cfg.popSize): # the parentSprites makes it so that if weights are passed, the parent sprites are kept in the new generation (done below this loop)
        size = random.randint(cfg.sizeRange[0], cfg.sizeRange[1])
        # bug fix! be careful with immutable objects (dict in this case)... must process random nums first
        
        speed = [speedX, speedY]
        spawnSpeed = [speedX, speedY] 
        detection = cfg.detectionParam
        colourImage = random.randint(cfg.colourImageRange[0], cfg.colourImageRange[1])

        if weightInit: 
            newSprite = Sprite(size, speed, detection, colourImage, [xSpawn,ySpawn], cfg.startScore, i, weightInit[weightIndexRand], None)
            weightIndexRand += 1 
            if weightIndexRand > len(weightInit) - 1: weightIndexRand = 0 # select a different init weight for each child sprite

        else:
            newSprite = Sprite(size, speed, detection, colourImage, [xSpawn,ySpawn], cfg.startScore, i, [], None)

        allSprites.add(newSprite)  # add sprite to sprite group allSprites
    
        if displayAll: isBeingDisplayed = True
        else: isBeingDisplayed = False
        spriteInfo.update({i:[[xSpawn, ySpawn], size, speed, spawnSpeed, detection, colourImage, cfg.startScore, isBeingDisplayed]}) 
        # SPRITEINFO DICT ORDER: spawnCoords(0), size(1), speed(2), spawnSpeed(3), detection(4), colourImage(5), score(6), isBeingDisplayed(7)
        # this dictionary keeps track of useful information about each sprite, assigned a unique ID when being generated

def generatePopulationFromFile(savedNN): # generate a population using weights in pickle file
    global allSprites
    global spriteInfo
    spriteInfo = {}
    allSprites = pygame.sprite.Group() # creates sprite group allSprites

    xSpawn = random.randint(cfg.xSpawnRange[0], cfg.xSpawnRange[1])
    ySpawn = random.randint(cfg.ySpawnRange[0], cfg.ySpawnRange[1])
    speedX = random.randint(cfg.speedRange[0], cfg.speedRange[1])
    speedY = random.randint(cfg.speedRange[0], cfg.speedRange[1])

    for i in range(len(savedNN)): # the -parentSprites makes it so that if weights are passed, the parent sprites are kept in the new generation (done below this loop)
        size = random.randint(cfg.sizeRange[0], cfg.sizeRange[1])
        # bug fix! be careful with immutable objects (dict in this case)... must process random nums first

        speed = [speedX, speedY]
        spawnSpeed = [speedX, speedY] 
        detection = cfg.detectionParam
        colourImage = random.randint(cfg.colourImageRange[0], cfg.colourImageRange[1])
        newSprite = Sprite(size, speed, detection, colourImage, [xSpawn,ySpawn], cfg.startScore, i, [], savedNN[i])
        allSprites.add(newSprite)  # add sprite to sprite group allSprites
    
        if displayAll: isBeingDisplayed = True
        else: isBeingDisplayed = False
        spriteInfo.update({i:[[xSpawn, ySpawn], size, speed, spawnSpeed, detection, colourImage, cfg.startScore, isBeingDisplayed]}) 
        # SPRITEINFO DICT ORDER: spawnCoords(0), size(1), speed(2), spawnSpeed(3), detection(4), colourImage(5), score(6), isBeingDisplayed(7)
        # this dictionary keeps track of useful information about each sprite, assigned a unique ID when being generated

def generatePopulationWithCrossover(parentWeights): # generate a population based on two parent sprites, by implementing crossover of the weights of each neural network
    global allSprites
    global spriteInfo
    print("[!] Applying crossover algorithm")
    spriteInfo = {}
    allSprites = pygame.sprite.Group() # creates sprite group allSprites

    xSpawn = random.randint(cfg.xSpawnRange[0], cfg.xSpawnRange[1])
    ySpawn = random.randint(cfg.ySpawnRange[0], cfg.ySpawnRange[1])
    speedX = random.randint(cfg.speedRange[0], cfg.speedRange[1])
    speedY = random.randint(cfg.speedRange[0], cfg.speedRange[1])

    for i in range(cfg.popSize): # the parentSprites makes it so that if weights are passed, the parent sprites are kept in the new generation (done below this loop)
        size = random.randint(cfg.sizeRange[0], cfg.sizeRange[1])
        # bug fix! be careful with immutable objects (dict in this case)... must process random nums first
        
        speed = [speedX, speedY]
        spawnSpeed = [speedX, speedY] 
        detection = cfg.detectionParam
        colourImage = random.randint(cfg.colourImageRange[0], cfg.colourImageRange[1])

        if parentWeights: 
            print("[!] Generating new sprite")
            newSprite = Sprite(size, speed, detection, colourImage, [xSpawn,ySpawn], cfg.startScore, i, parentWeights, None)

        else:
            newSprite = Sprite(size, speed, detection, colourImage, [xSpawn,ySpawn], cfg.startScore, i, [], None)

        allSprites.add(newSprite)  # add sprite to sprite group allSprites
    
        if displayAll: isBeingDisplayed = True
        else: isBeingDisplayed = False
        spriteInfo.update({i:[[xSpawn, ySpawn], size, speed, spawnSpeed, detection, colourImage, cfg.startScore, isBeingDisplayed]}) 
        # SPRITEINFO DICT ORDER: spawnCoords(0), size(1), speed(2), spawnSpeed(3), detection(4), colourImage(5), score(6), isBeingDisplayed(7)
        # this dictionary keeps track of useful information about each sprite, assigned a unique ID when being generated

def generateCheckpoints():
    global allCheckpoints
    global checkpointInfo
    checkpointInfo = {}
    allCheckpoints = pygame.sprite.Group()  # creates sprite group allCheckpoints
    for i in range(cfg.numCheckpoints):
        xSpawn = random.randint(cfg.xCPSpawnRange[0], cfg.xCPSpawnRange[1])
        ySpawn = random.randint(cfg.yCPSpawnRange[0], cfg.yCPSpawnRange[1])
        newCheckpoint = Checkpoint([xSpawn, ySpawn], i)
        allCheckpoints.add(newCheckpoint) # add checkpoint to group allCheckpoints
        checkpointInfo.update({i:[[xSpawn, ySpawn], [], {}]})
        # CHECKPOINT INFO ORDER: spawnCoords(0), list of sprites that have collided with it(1), dict of sprites that have come within range, and their range(2) - ranges defined in cfg.spriteProximityScores
        # this dictionary keeps track of useful information about each checkpoint, assigned a unique ID when being generated.

if __name__ in '__main__':
    title.printTitle()
    UI().run()

#TODO:
# implement crossover algorithm
# make each "map" equal difficulty
