import os
import random
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # quiet down TF 
import tensorflow as tf
from tensorflow.keras import layers
import config as cfg

class NN():

    def constructor(self, input_nodes, hidden_nodes, hidden_nodes_2, output_nodes): 
        # generates completely random weights 
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.hidden_nodes_2 = hidden_nodes_2
        self.output_nodes = output_nodes
        
        self.input_weights = tf.random.normal([self.input_nodes, self.hidden_nodes])
        self.output_weights = tf.random.normal([self.hidden_nodes, self.hidden_nodes_2])
        self.output_weights_2 = tf.random.normal([self.hidden_nodes_2, self.output_nodes])

    def constructorWithInitWeight(self, input_nodes, hidden_nodes, hidden_nodes_2, output_nodes, weightInit):
        # mutates weights based on previous weights, passed in the list weightInit
        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.hidden_nodes_2 = hidden_nodes_2
        self.output_nodes = output_nodes
        # generate tensors with random values taken from a NDist, to be added to the init weights (mutate them a bit)
        input_weights, output_weights, output_weights_2 = weightInit[0], weightInit[1], weightInit[2]
        input_weights_mutator = tf.random.normal([self.input_nodes, self.hidden_nodes], mean=cfg.mutationNDistMean, stddev=cfg.mutationNDistStdDev)
        output_weights_mutator = tf.random.normal([self.hidden_nodes, self.hidden_nodes_2], mean=cfg.mutationNDistMean, stddev=cfg.mutationNDistStdDev)
        output_weights_2_mutator = tf.random.normal([self.hidden_nodes_2, self.output_nodes], mean=cfg.mutationNDistMean, stddev=cfg.mutationNDistStdDev)

        self.input_weights = tf.math.add(input_weights, input_weights_mutator)
        self.output_weights = tf.math.add(output_weights, output_weights_mutator)
        self.output_weights_2 = tf.math.add(output_weights_2, output_weights_2_mutator)
    
    def constructorCrossover(self, input_nodes, hidden_nodes, hidden_nodes_2, output_nodes, parentWeights):
        # mutates weights based on two parent weights
        # parent weights must be a list [parentWeights1, parentWeights2]

        self.input_nodes = input_nodes
        self.hidden_nodes = hidden_nodes
        self.hidden_nodes_2 = hidden_nodes_2
        self.output_nodes = output_nodes

        # Crossover:
        newWeights = []
        for layer in range(len(parentWeights[0])):
            newLayer = []
            for x in range(len(parentWeights[0][layer])):
                newX = []
                for y in range(len(parentWeights[0][layer][x])):
                    
                    n = random.randint(1, 10) # 50% chance of weight value coming from each parent
                    if 1 <= n <= 6: # take value from parent 1
                        newY = parentWeights[0][layer][x][y].numpy()
                        newX.append(newY)
                    else: # take value from parent 2
                        newY = parentWeights[1][layer][x][y].numpy()
                        newX.append(newY)
                        
                newLayer.append(newX)
            newWeights.append(tf.constant(newLayer, dtype="float32"))
        
        input_weights, output_weights, output_weights_2 = newWeights[0], newWeights[1], newWeights[2]
        
        # Add variation:
        input_weights_mutator = tf.random.normal([self.input_nodes, self.hidden_nodes], mean=cfg.mutationNDistMean, stddev=cfg.mutationNDistStdDev)
        output_weights_mutator = tf.random.normal([self.hidden_nodes, self.hidden_nodes_2], mean=cfg.mutationNDistMean, stddev=cfg.mutationNDistStdDev)
        output_weights_2_mutator = tf.random.normal([self.hidden_nodes_2, self.output_nodes], mean=cfg.mutationNDistMean, stddev=cfg.mutationNDistStdDev)
        
        self.input_weights = tf.math.add(input_weights, input_weights_mutator)
        self.output_weights = tf.math.add(output_weights, output_weights_mutator)
        self.output_weights_2 = tf.math.add(output_weights_2, output_weights_2_mutator)
                
    def getWeights(self):
        return [self.input_weights, self.output_weights, self.output_weights_2] # returns weight tensors for this NN

    def predict(self, input_data):
        self.input_layer = tf.constant(input_data, shape=(1, self.input_nodes), dtype="float32")
        self.hidden_layer = tf.linalg.matmul(self.input_layer, self.input_weights)
        self.hidden_layer = tf.math.sigmoid(self.hidden_layer) # activation
        self.hidden_layer_2 = tf.linalg.matmul(self.hidden_layer, self.output_weights)
        self.hidden_layer_2 = tf.math.sigmoid(self.hidden_layer_2)
        self.output_layer = tf.linalg.matmul(self.hidden_layer_2, self.output_weights_2)
        self.output_layer = tf.nn.softmax(self.output_layer)
        output = self.output_layer

        return output
